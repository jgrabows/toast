
# Configuration file for generating a Lb2LcD0K toy sample with a bunch of Ds* and Xic states
#
# Alessio Piucci

import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const

# non-resonant component, just a little bit
non_res = MatrixElement.NonResonant(nonres_size = Const(0.06))

# M and d1 polarisation factors
M_d1_hel = None

#M_d1_hel = {}
#M_d1_hel[1] = {}
#M_d1_hel[1][1] = Const(0.5)
#M_d1_hel[1][-1] = Const(0.5)
#
#M_d1_hel[-1] = {}
#M_d1_hel[-1][1] = Const(0.5)
#M_d1_hel[-1][-1] = Const(0.5)

# list of Lc D0 resonances
A = [
]

# list of D0 K resonances
B = [
    
    # Lb --> Lc Ds2*(2573)
    MatrixElement.BResonanceBreitWigner(name = Ds2_2573.name, label = Ds2_2573.label,
                                        mass = Ds2_2573.mass, width = Ds2_2573.width,
                                        
                                        spin = Ds2_2573.spin, parity = Ds2_2573.parity, aboveLmin_M = 0,
                                        #res_helicity_factors_reduced = True,
                                        #res_helicity_factors_dict = {
                                        #    # reduced
                                        #    "-2" : Const(1.),
                                        #    "0" : Const(1.),
                                        #    "2" : Const(1.)
                                        #},
                                        BLS_couplings_dict_M = {
                                            "Ds2_2573__M_Bs3l2_Real" : Const(1.),
                                            "Ds2_2573__M_Bs3l2_Imag" : Const(0.),
                                        },
                                        BLS_couplings_dict_res = {
                                            "Ds2_2573__res_Bs0l4_Real"  : Const(0.2),
                                            "Ds2_2573__res_Bs0l4_Imag"  : Const(0.1),
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kAzure, linestyle = 7, fillstyle = 3954),
    
    # Lb --> Lc Ds*1(2700)
    MatrixElement.BResonanceBreitWigner(name = Ds1_2700.name, label = Ds1_2700.label,
                                        mass = Ds1_2700.mass, width = Ds1_2700.width,
                                        spin = Ds1_2700.spin, parity = Ds1_2700.parity, aboveLmin_M = 0,
                                        #res_helicity_factors_reduced = True,
                                        #res_helicity_factors_dict = {
                                        #    # reduced
                                        #    "-2" : Const(1.),
                                        #    "0" : Const(1.),
                                        #    "2" : Const(1.)
                                        #},
                                        BLS_couplings_dict_M = {
                                            "Ds1_2700__M_Bs1l0_Real" : Const(1.),
                                            "Ds1_2700__M_Bs1l0_Imag" : Const(0.),
                                        },
                                        BLS_couplings_dict_res = {
                                            "Ds1_2700__res_Bs0l2_Real"  : Const(0.5),
                                            "Ds1_2700__res_Bs0l2_Imag"  : Const(0.)
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kBrown, linestyle = 7, fillstyle = 3954),
    
]

# list of Lc K resonances
C = [

    # Lb --> Xic(2815) D0
    MatrixElement.CResonanceBreitWigner(name = Xic_2815.name, label = Xic_2815.label,
                                        mass = Xic_2815.mass, width = Const(0.030),
                                        spin = Xic_2815.spin, parity = Xic_2815.parity, aboveLmin_M = 0,
                                        BLS_couplings_dict_M = {
                                            "Xic_2815__M_Bs3l2_Real" : Const(0.6),
                                            "Xic_2815__M_Bs3l2_Imag" : Const(0.2),
                                        },
                                        BLS_couplings_dict_res = {
                                            "Xic_2815__res_Bs1l4_Real"  : Const(0.5),
                                            "Xic_2815__res_Bs1l4_Imag"  : Const(0.1)
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kMagenta, linestyle = 7, fillstyle = 3954),
]
