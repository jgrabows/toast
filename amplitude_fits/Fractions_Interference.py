#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
# ==============================================================================

import math
import sys
import argparse
import numpy as np
import importlib
import itertools

import tensorflow as tf

# import the TensorflowAnalysis libraries
import Interface
import Optimisation
from Interface import Const, Complex

# import the local libraries
import MatrixElement

from Constants import *

# import the Python settings
from python_settings import *

# helicity factors for the Lb and Lc
M_d1_hel = None

# compute and write the fit fractions, with the related uncertainties
def WriteFitFractionsWithErrors(sess, phsp, pdf, data_placeholder,
                                switches, norm_sample,
                                resonances, outFile_name, errors = False, results = None):
    
    # compute the fit fractions and return the total PDF integral as well
    fit_fractions, total_int = Optimisation.CalculateFitFractions(sess, pdf, data_placeholder,
                                                                  switches, norm_sample, errors = errors)
        
    # list of resonance names
    resonance_names = [res.name for res in resonances]
    
    # write the fit fractions in the output file
    Optimisation.WriteFitFractionsWithErrors(fit_fractions,
                                             resonance_names +  ["non-resonant"],
                                             outFile_name + "_fractions.info",
                                             results = results)

    return fit_fractions, total_int

# compute and write out the interference terms
def WriteInterferences(sess, phase_space, total_int, norm_sample,
                       resonances, outFile_name):

    # list of resonance names
    resonance_names = [res.name for res in resonances]
    
    # create all possible 2-states combinations
    res_combinations = list(itertools.combinations(resonance_names, 2))
    
    # create a dictionary of {2-states combinations : interference amplitude}
    interference_terms = {}
    
    for res in res_combinations:
        interference_terms.update({res : Interface.CastComplex(0.)})

    # now get the 2-states interference amplitudes
    for states, data_pdf_interference in interference_terms.iteritems():
        
        # get the indeces related to the current states
        component_indeces = [resonance_names.index(states[0]),
                             resonance_names.index(states[1])]
        
        # compute the interference amplitude
        data_pdf_interference = MatrixElement.MatrixElement_interference(phase_space, phase_space.data_placeholder,
                                                                         component_indeces, resonances,
                                                                         M_d1_hel_ = M_d1_hel)
        
        # update the dictionary
        interference_terms[states] = data_pdf_interference
        
    # compute the interference terms
    interference_terms = Optimisation.CalculateInteferenceTerms(sess, total_int, phase_space.data_placeholder,
                                                                data_pdf_interference, interference_terms,
                                                                norm_sample = norm_sample)
    
    # write the interference terms in the output file
    Optimisation.WriteInterferenceTerms(interference_terms,
                                        outFile_name + "_interference.info")

    # convert the structure of the interference terms
    # in a dict of dict, that will be used to write out it as latex table
    interference_terms_dict = {}

    # loop over the resonance names
    for res_name_1 in resonance_names :

        interference_terms_dict[res_name_1] = {}
        
        for res_name_2 in resonance_names :

            # well, interference of a state with itself...
            if res_name_1 == res_name_2 :
                continue

            # now search for the current combination of res_1 and res_2
            for states, interference_term in interference_terms.iteritems():

                if (states[0] == res_name_1) and (states[1] == res_name_2) :
                    interference_terms_dict[res_name_1][res_name_2] = interference_term
    
    return interference_terms_dict

######################

# write the fit fractions as latex table
def WriteFitFractionsLatex(resonance_names, fit_fractions, outFile_name,
                           print_errors = False) :

    # open the output file
    outFile = open(outFile_name, "w")

    # open the table
    s = "\\begin{table}[h]\n"
    s += "\\begin{center}\n"

    # two columns: component of the amplitude model, and fit fraction
    s += "\\begin{tabular}{c | c}";

    s += "\\toprule\n"

    s += " Component & Fit fraction \\\\\n"
    s += "\\midrule\n"

    outFile.write(s)
    
    # loop over the resonances and the fit fractions
    for name, value in zip(resonance_names, fit_fractions) :
        
        # build and write the line with the current fit fraction
        s = ""

        # [0] = fit fraction, [1] = uncertainty of the fit fraction
        if print_errors :
            s = "    ${}$ & {} \\\\ \n".format(name, round(value[0], 4))
        else :
            s = "    ${}$ & {} \\pm {} \\\\ \n".format(name, round(value[0], 4), round(value[1], 4))
            
        outFile.write(s)

    # close the table
    s = "\\bottomrule\n"
    s += "\\end{tabular}\n"
    s += "\\end{center}\n"
    s += "\\caption{A nice table created automatically.}\n"
    s += "\\label{tab:fit_results}\n"
    s += "\\end{table}\n"

    outFile.write(s)

    # close the output file
    outFile.close()

    # https://i.kym-cdn.com/photos/images/original/001/240/075/90f.png
    return

# write the parameter correlations/covariances or the interference terms out of the fit
# more in general, write out any kind of result wich is stored as dict
# and has to be put in a matrix
def WriteMatrixResultsLatex(results_dict, outFile_name) :
    
    # open the output file
    outFile = open(outFile_name, "w")

    # open the table
    s = "\\begin{table}[h]\n"

    # you want to resize the tables: I put the command there but I comment it
    # uncomment it directly in the Tex if you really need that
    s += "%\\resizebox{1.5\textwidth}{!}{\n"
    s += "\\begin{center}\n"
    s += "\\begin{tabular}{c |"
    
    # I have to add the specify number of columns = number of parameters
    for i in range(len(results_dict)) :
      s += " c ";

    s += "}\n"
    s += "\\toprule\n"

    # ordered list of parameter names,
    # to ensure that the order how they are put in the matrix is correct,
    # for each of the parameters
    ordered_names = []
    
    # now it's time for the name of the parameters,
    # to be placed on top of the table
    for param_name in results_dict.keys() :
        s += " & ${}$ ".format(param_name)
        ordered_names += [param_name]
        
    s += " \\\\\n"
    s += "\\midrule\n"

    outFile.write(s)
    
    # first loop over the parameters
    for param_name in results_dict.keys() :
        
        # write the name of the current parameter
        s = "    ${}$ \t ".format(param_name)

        # second loop over all the parameters!
        for param_name_2 in ordered_names :
            
            # retrieve the result from the dict
            # or, if the dict is messed up, replace it with a slash
            try :
                s += " & {}".format(round(results_dict[param_name][param_name_2], 4))
            except :
                s += " & / "

        s += " \\\\\n"
        
        outFile.write(s)

    # let's close the table
    s = "\\bottomrule\n"
    s += "\\end{tabular}\n"
    s += "\\end{center}\n"

    # this is related to the resizebox command (see above), commented by default
    s += "%}\n"

    s += "\\caption{A nice huge table created automatically.}\n"
    s += "\\label{tab:correlations}\n"
    s += "\\end{table}\n"

    outFile.write(s)
        
    # close the output file
    outFile.close()

    # https://i.kym-cdn.com/photos/images/original/001/240/075/90f.png
    return

######################

# compute the uncertainties of the fit fractions
def GetFitFractionsErrors(sess, phsp, datapoint, resonances, norm_sample):

    fit_fractions_uncertainties = []

    ############
    
    # ONLY TEMP:
    # set unvalid errors, since the analytical derivation must be revised...
    for res in resonances :
        fit_fractions_uncertainties += [0.]

    return fit_fractions_uncertainties
    
    ############
    
    # first compute the denominator, which is common to all the resonances
    denominator = sess.run(Interface.Sqrt(Const(len(norm_sample))))

    logging.debug("GetFitFractionsErrors::denominator = {}".format(denominator))
    
    # now let's start with the fun, computing the numerator for each of the resonances
    
    # loop over the resonances
    for res in resonances :
        
        # get the values of the amplitude variables
        #coordinates = res.get_coordinates(phsp, datapoint)

        # get the TF variables
        tfpars = tf.trainable_variables()

        # append variables which are matching with the fit parameters to a list
        list_variables = [ p for p in tfpars if p.floating() ]
        
        ## stop_gradients it used to compute partial derivatives, rather then total derivatives
        #gradients = tf.gradients(fitfraction_graph,
        #                         list_variables,
        #                         [fit_variables in a array ],
        #                         stop_gradients = [fit_variables in a array])
        #
        ## sum up the partial derivatives, multiplicated by the correlations
        #sum = Const(0.)
        # 
        #for index, var in zip(list_variables) :
        #    for index_2, var_2 in zip(list_variables) :
        #        sum += gradients[index] * gradients[index_2] * corr[index][index_2]
        #        
        #numerator = sess.run(Sqrt(sum))
        numerator = Const(0.)
        
        #######################

        # finally compute this damned uncertainty
        fit_fraction_error = numerator / denominator

        # if the uncertainty is nan for whatever reason,
        # returns an unvalid value
        if math.isnan(fit_fraction_error) :
            fit_fraction_error = -1.
        
        # finally compute this damned uncertainty
        fit_fractions_uncertainties += [fit_fraction_error]

    return fit_fractions_uncertainties
