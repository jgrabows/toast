#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
#         (based on previous software by Adam Morris)
# ==============================================================================

import tensorflow as tf
import ROOT
import sys
import math

#import the local libraries
import Kinematics
import Dynamics
import KMatrix

from Optimisation import FitParameter, PrintParameters
import Interface
from Interface import Const, Complex, CastComplex, Acos, Clebsch, Ones, Sqrt, Abs

import Utils
from Constants import *

# import the Python settings
from python_settings import *

########
# DISCLAIMER:
# The current implementation of the matrix elements
# rely on the fact that you have a (spin 1/2) --> (spin 1/2) scalar scalar decay,
# with d1 being the daughter particle with non null spin
# If you try to use this exact implementation of the code for a decay
# with different spin structures... well do that at your own risk!
########
                
######

# some global variables
sess = None

fitted_values = None

M_d1_hel = None

parsed_parameters = {}

################

# parse the operations between parameters
def parse_operation_parameter(param_name, opparam_string) :

        # check if the input argument is a string (= it has to be parsed) or a parameter/Const (keep it as it is)
        if not isinstance(opparam_string, basestring) :
                
                # standard FitParameter or Const, return it
                return opparam_string
        else :
                # parameter to parse!
                # save the operation in the dedicated list,
                # and parse it

                global parsed_parameters
                
                parsed_parameters[param_name] = opparam_string

                return Utils.ParseOpParameters(opparam_string)

# parse the M and d1 helicity factors:
# a little bit cumbersome, but whatever
def Parse_M_d1_helicities(M_d1_helicity_factors) :
    
    # loop over the M and d1 helicities
    for M_hel in M_d1_helicity_factors.keys():
            for d1_hel in M_d1_helicity_factors[M_hel].keys():

                    # check if the parameter is a string (= it has to be parsed) or a is parameter/Const (keep it as it is)
	            if not isinstance(M_d1_helicity_factors[M_hel][d1_hel], basestring) :
                            continue
                    else :
                            # parameter to parse!
                            # save the operation in the dedicated list,
		            # and parse it
                            
                            global parsed_parameters
                            
		            parsed_parameters["Mhel_" + str(M_hel) + "_d1hel_" + str(d1_hel)] = M_d1_helicity_factors[M_hel][d1_hel]

                            M_d1_helicity_factors[M_hel][d1_hel] = Utils.ParseOpParameters(M_d1_helicity_factors[M_hel][d1_hel])

    return M_d1_helicity_factors

################

# get all the possible B_LS couplings for the A --> B C decay
# you have to reduce the number of needed couplings later!
def get_all_possible_BLS(J_A, J_B, J_C,
                         
                         # does the decay conserve parity?
                         parity_conserved = False,
                         P_A = +1, P_B = +1, P_C = +1):
        
        L_values = []
        
        # get the S values of the daughters combination
        S_values = Utils.GetSvalues(J_B, J_C)
        
        B_SL = {}
        
        # loop over the allowed S values of the daughters combination
        # I first loop over S, because I need it to compute the L values
        for S in S_values:

                B_SL[S] = {}
                
                # loop over the allowed L = J_A + S of the decay
                for L in range(abs(J_A - S), J_A + S + 1, 2):
                        
                        # if the parity is conserved in the decay,
                        # check that the current L is allowed
                        if parity_conserved :
                                if (P_B * P_C * (-1)**(L/2)) != P_A:
                                        continue
                        
                        L_values += [L]

                        B_SL[S][L] = Coupling()
                                
        return S_values, L_values, B_SL

##############################

# class for the couplings
class Coupling:
        def __init__(self, name = "",
                     comp_1 = Const(1.), comp_2 = Const(0.),
                     couplings_RealAndImag = True,
                     couplings_MagAndPhase = False,
                     comp_1_ind = True, comp_2_ind = True):

                self.name = name
                
                self.comp_1 = comp_1
                self.comp_2 = comp_2
                
                # some flags to say if the components are free fit parameters (= True)
                # or if are set equal to other parameters (= False)
                self.comp_1_ind = comp_1_ind
                self.comp_2_ind = comp_2_ind
                
                if couplings_RealAndImag :
                        self.comp_1_name = name + "_Real"
                        self.comp_2_name = name + "_Imag"
                elif couplings_MagAndPhase :
                        self.comp_1_name = name + "_Mag"
                        self.comp_2_name = name + "_Phase"
                        
                self.couplings_RealAndImag = couplings_RealAndImag
                self.couplings_MagAndPhase = couplings_MagAndPhase
                
                self.UpdateValue()

        # update the value of the coupling,
        # from the values of the single components
        def UpdateValue(self):

                if self.couplings_MagAndPhase :
                        self.value = Interface.Polar(self.comp_1, self.comp_2)
                elif self.couplings_RealAndImag :
                        self.value = Interface.Complex(self.comp_1, self.comp_2)
                else :
                        logging.error("Coupling: Error, no valid mode of using the BLS is selected")
                        exit(1)

                return

# class for the helicity amplitudes, with the BLS representation of the couplings
class BLSHelicityAmplitude():
        def __init__(self,
                     J_mother = None,
                     J_1 = None, hel_1 = None,
                     J_2 = None, hel_2 = None,
                     B_SL = {},

                     # do you want to set the helicity amplitude value to a constant?
                     fix_to_const = None,
                     
                     # do you want to fix the amplitude value to some other amplitude?
                     # If so, fill a 2-element list with [ampl, mult_factor]
                     # and the current amplitude will take the value ampl.value * mult_factor
                     fix_value = []):
                
                # spin of the decaying particle/state
                self.J_mother = J_mother

                # spin and helicity values of the decay daughters
                self.J_1 = J_1
                self.hel_1 = hel_1

                self.J_2 = J_2
                self.hel_2 = hel_2
                
                # B_SL couplings
                self.B_SL = B_SL

                self.fix_to_const = fix_to_const
                self.fix_value = fix_value
                        
        # get the value of the helicity amplitude,
        # by summing over the LS combinations and the B_SL couplings
        def getvalue(self):

                if self.fix_to_const != None :
                        return self.fix_to_const
                
                elif self.fix_value == [] :

                        # compute the value summing over the B_LS couplings
                        
                        HelAmp = CastComplex(Const(0.))
                        
                        # loop over the S, L and resonance helicity values
                        for S in self.B_SL.keys():
                                for L in self.B_SL[S].keys():
                                                                                        
                                        HelAmp += (self.B_SL[S][L].value * math.sqrt((L + 1.)/(self.J_mother + 1.)) *
                                                   complex(Clebsch(self.J_1, self.hel_1,
                                                                   self.J_2, -self.hel_2,
                                                                   S, self.hel_1 - self.hel_2),
                                                           0) *
                                                   complex(Clebsch(L, 0,
                                                                S, self.hel_1 - self.hel_2,
                                                                self.J_mother, self.hel_1 - self.hel_2),
                                                        0)
                                        )
                                        
                        return HelAmp
                else :
                        # return the ampl.value * multiplicative factor
                        return CastComplex(self.fix_value[0].getvalue() * self.fix_value[1])

                
##############################

# build (by hand...) the combinations of helicities which are allowed,
# depending on the M and d1 helicities
res2scalars_allowed_helicities = {}
res2scalars_allowed_helicities[+1] = {}
res2scalars_allowed_helicities[+1][+1] = [2, 0]
res2scalars_allowed_helicities[+1][-1] = [0, -2]

res2scalars_allowed_helicities[-1] = {}
res2scalars_allowed_helicities[-1][+1] = [2, 0]
res2scalars_allowed_helicities[-1][-1] = [0, -2]

res2halfAndscalar_allowed_helicities = {}
res2halfAndscalar_allowed_helicities[+1] = {}
res2halfAndscalar_allowed_helicities[+1][+1] = [1, -1]
res2halfAndscalar_allowed_helicities[+1][-1] = [1, -1]

res2halfAndscalar_allowed_helicities[-1] = {}
res2halfAndscalar_allowed_helicities[-1][+1] = [1, -1]
res2halfAndscalar_allowed_helicities[-1][-1] = [1, -1]

# aux method to get the allowed helicity values of the resonance,
# depending on the M and d1 helicities
def get_res_allowed_helicities(hel_M, hel_d1, res_spin) :

        # ok, that's simple:
        # if the resonance has spin 0, the only helicity value that can be taken is null
        if res_spin == 0:
                return [0]
        
        # check if the resonance decays to two scalars (half-integer spin)
        # or 1/2 + scalar (integer spin)
        if res_spin % 2 == 0 :
                # integer spin
                return res2scalars_allowed_helicities[hel_M][hel_d1]
        else :
                # half-integer spin
                return res2halfAndscalar_allowed_helicities[hel_M][hel_d1]
        
# get all the possible values that can be taken by the resonance helicities
def get_all_possible_res_helicities(res_spin) :

        all_res_helicities = []
        
        for hel_M in M_helicities :
                for hel_d1 in d1_helicities :

                        for hel_res in get_res_allowed_helicities(hel_M = hel_M,
                                                                  hel_d1 = hel_d1,
                                                                  res_spin = res_spin) :
                                
                                # if the helicity of the resonance
                                # is greater than its spin, skip it
                                if abs(hel_res) > res_spin:
                                        continue
                                else :
                                        all_res_helicities += [hel_res]

        # remove the duplicates 
        all_res_helicities = list(set(all_res_helicities))

        logging.debug("get_all_possible_res_helicities: all_res_helicities = {}".format(all_res_helicities))
        
        return all_res_helicities

##############################

# base class for all resonances
class Resonance:
	def __init__(self, name, label, tex_name = "",
                     mass = 1., width = 0.1,
                     spin = 0, parity = +1,
                     aboveLmin_M = 0,
                     aboveLmin_res = 0,
                     #
                     # for the Flatte' lineshape
                     ma2 = None,
                     mb2 = None,
                     #
                     # to fix a Lb or a res helicity amplitude (only for B resonances),
                     # to fix the overall magnitude and phase conventions
                     fix_Lb_hel_amplitude = None,
                     fix_Bres_hel_amplitude = None,
                     #
                     # to fix --> all <-- the helicity amplitdes to a const value
                     fix_all_hel_amplitudes = None,
                     #
                     # couplings
                     use_BLS = True,
                     couplings_RealAndImag = True,
                     couplings_MagAndPhase = False,
                     #
                     # explicit helicity factors used with the BLS,
                     # to take into account resonance polarisation
                     res_helicity_factors_reduced = True,
                     res_helicity_factors_dict = {},
                     #
                     # BLS couplings
                     BLS_couplings_dict_M = [],
                     BLS_couplings_dict_res = [],
                     #
                     # generic couplings, used alternatively to the BLS representation
                     generic_couplings_dict = {},
                     #
                     resdecay_parity_conservation = False,
                     colour = 1, linewidth = 2, linestyle = 1, fillstyle = 0,

                     # coefficients of the polynomial line shape
                     pol_coefficients = [],

                     # for the K-matrix
                     Kmat__num_channels = 0,  # number of channels contributing
                     Kmat__channel = 0,       # channel of which you want to get the amplitude
                     Kmat__resonance_masses = [],
                     Kmat__alpha_couplings = [],
                     Kmat__g_couplings = [],
                     Kmat__width_factors = [],
                     Kmat__daughters_masses = [],
                     Kmat__L_channels = [],
                     Kmat__background_terms = []
        ):

                # let's repeat the disclaimer:
                ########
                # The current implementation of the matrix elements 
                # rely on the fact that you have a (spin 1/2) --> (spin 1/2) scalar scalar decay,
                # with d1 being the daughter particle with non null spin
                # If you try to use this exact implementation of the code for a decay
                # with different spin structures... well do that at your own risk!
                ########
                
                logging.info("")
	        logging.info("Resonance:: Creating resonance {}".format(name))

                # ask for non-zero spin, and |parity| = 1
                #assert spin > 0 and abs(parity) is 1

                # ask for |parity| = 1
                assert abs(parity) is 1
                
		self.name = name
                self.label = label
                self.tex_name = tex_name
                
                # parse the mass and width:
                # the names are only used to five meaningful names, in case the paramaters must be parsed
                mass = parse_operation_parameter(self.name + "__mass", mass)
		width = parse_operation_parameter(self.name + "__width", width)
                
                self.mass = mass
                self.width = width
                
                # quantum numbers of the state
		self.spin = spin
		self.parity = parity

                self.aboveLmin_M = aboveLmin_M
                self.aboveLmin_res = aboveLmin_res
                
                self.resdecay_parity_conservation = resdecay_parity_conservation

                # for the Flatte' lineshape
                self.ma2 = ma2
                self.mb2 = mb2
                
                # fix a Lb or a res helicity amplitude,
                # to fix the overall magnitude and phase conventions
                #
                # for the Lb amplitude:
                # the expected format is None, or a pair ("helres:helspect", value)
                # ":helspect" is there only if the spectator particle is not a scalar
                # for the res amplitude:
                # this can only be set for B-resonances, and it's "just" a complex number,
                # since the B daughters are scalars
                self.fix_Lb_hel_amplitude = fix_Lb_hel_amplitude
                self.fix_Bres_hel_amplitude = fix_Bres_hel_amplitude

                # to fix --> all <-- the helicity amplitudes to a const value
                self.fix_all_hel_amplitudes = fix_all_hel_amplitudes
                
                # do you want to use the BLS or generic representation for the couplings?
                self.use_BLS = use_BLS
                
                # set the convention of the couplings
                self.couplings_MagAndPhase = couplings_MagAndPhase
                self.couplings_RealAndImag = couplings_RealAndImag
                
                # explicit helicity factors used with the BLS,                                                                                                                                  
                # to take into account resonance polarisation
                self.res_helicity_factors_reduced = res_helicity_factors_reduced
                self.res_helicity_factors_dict = res_helicity_factors_dict
                self.res_helicity_factors = {}  # it will be set later

                # generic (helicity) couplings, used alternatively to the BLS representation
                self.generic_couplings_dict = generic_couplings_dict
                
                # BLS couplings
                self.BLS_couplings_dict_M = BLS_couplings_dict_M
                self.BLS_couplings_dict_res = BLS_couplings_dict_res

                # if BLS are not used, there is no need of the explicit helicity factors of the resonance
                # also, clean all the BLS couplings.
                # The other way around for the generic couplings
                if not self.use_BLS :
                        self.res_helicity_factors_dict = {}
                        self.BLS_couplings_dict_M = {}
                        self.BLS_couplings_dict_res = {}
		else :
                        self.generic_couplings_dict = {}
                        
                # some style
                self.colour = colour
		self.linewidth = linewidth
		self.linestyle = linestyle
		self.fillstyle = fillstyle

                # coefficients of the polynomial line shape
                self.pol_coefficients = pol_coefficients

                #############

                # for the K matrix
                self.Kmat = None
                self.Kmat__num_channels	= Kmat__num_channels
                self.Kmat__channel = Kmat__channel
                self.Kmat__resonance_masses = Kmat__resonance_masses
                self.Kmat__alpha_couplings = Kmat__alpha_couplings
                self.Kmat__g_couplings = Kmat__g_couplings
                self.Kmat__width_factors = Kmat__width_factors
                self.Kmat__daughters_masses = Kmat__daughters_masses
                self.Kmat__L_channels = Kmat__L_channels
                self.Kmat__background_terms = Kmat__background_terms

                self.Kmat__num_res = len(self.Kmat__resonance_masses)
                
                #############
                
                # used for the resonance shape
		self.m_ancestor = M.mass    # M mass
                self.d_parent = Const(1.5)  # barrier factor radius of the M
		self.d_res = Const(5.0)     # barrier factor radius of the resonance

                # helicity amplitudes of the M and resonance decays,
                # with the generic representation of the couplings
                self.genericHelAmp_M = {}
                self.genericHelAmp_res = {}
                
                # helicity amplitudes of the M and resonance decays,
                # with the BLS representation of the couplings
                self.BLSHelAmp_M = {}
		self.BLSHelAmp_res = {}
                
                # B_LS couplings of the M and resonance decays
                self.BLS_M = {}
                self.BLS_res = {}
                
                # initialise the helicity amplitudes, couplings and some more variables
                # this is specific for every species of resonance,
                # so you can find the method implementation directly there
                self.initialise_resonance()
                
                self.set_res_typename()
                
        def set_helicities(self) :

                #######
                # set the helicities of the initial and final-state particles
                #######
                
                self.M_helicities = range(-M.spin, M.spin + 1, 2)
		self.a_helicities = range(-self.a.spin, self.a.spin + 1, 2)
                self.b_helicities = range(-self.b.spin, self.b.spin + 1, 2)
                self.spectator_helicities = range(-self.spectator.spin, self.spectator.spin + 1, 2)
                
                logging.debug(("Resonance::set_helicities: M_helicities = {}, a_helicities = {}, b_helicities = {},"
                               + " spectator_helicities = {}").format(self.M_helicities, self.a_helicities,
                                                                      self.b_helicities, self.spectator_helicities))

        # set the helicity amplitudes and the couplings
        def set_HelAmp_and_couplings(self):
                
                # check what's the complex convention of the couplings
                if self.couplings_MagAndPhase :
                        logging.info("Resonance:: Setting the couplings using mag and phase convention")
                elif self.couplings_RealAndImag :
                        logging.info("Resonance:: Setting the couplings using real and imaginary parts convention")
                else :
                        logging.error("Resonance:: Error: the convention to use with the {} coupling is not correctly set.")
                        exit(1)
                        
                # do you want to use the BLS or the generic representation for the couplings?
                if self.use_BLS :
                        self.set_HelAmp_and_couplings_BLS()
                else :
                        self.set_HelAmp_and_couplings_generic()

        # set the couplings with the generic representation
        def set_HelAmp_and_couplings_generic(self):

                # L_values = orbital angular momenta between the resonance daughters
                S_values, L_values, temp_variable = get_all_possible_BLS(self.spin, self.a.spin, self.b.spin,
                                                                         parity_conserved = self.resdecay_parity_conservation,
                                                                         P_A = self.parity, P_B = self.a.parity, P_C = self.b.parity)

                # L_parent = angular orbital momentum between the resonance and the spectator particle
		self.L_parent_min = min(Utils.GetLvalues_res_spectator(self.spin, self.spectator.spin))

                # L_res = angular orbital momentum between the resonance daughters
                self.L_res_min = min(L_values)
                
                self.genericHelAmp_M = {}
                self.genericHelAmp_res = {}

                # set the helicity amplitudes
                self.set_genericHelAmp()

                return
                
        # set the helicity amplitudes and the couplings with the BLS representation
        def set_HelAmp_and_couplings_BLS(self):
                
                #######
                # set the B_LS couplings
                #######
                
                # set the B_LS couplings for the M decay
                self.set_BLS_M()
                
                # set the B_LS couplings for the resonance decay
                self.set_BLS_res()

                #######
                # set the helicity amplitudes
                #######
                
                self.set_BLSHelAmp()
                
                #######
                # set the resonance helicity factors to take care of the resonance polarisation, if needed
                #######
                
                # reduced-model (i.e. only depending on the resonance helicity), or full model?
                if self.res_helicity_factors_reduced :

                        # loop over all the possible resonance helicities
                        for hel_res in get_all_possible_res_helicities(self.spin) :
				
				required_coupling = self.name + "__helcoupling_" + str(hel_res)
                                
		                if self.res_helicity_factors_dict != {} :
                                        # I need to set the helicity coupling
                                        try :
                                                # set/parse the paramater
                                                # the name is only used to give a meaningful name, in case the parameter must be parsed
                                                self.res_helicity_factors[hel_res] = parse_operation_parameter(self.name + "__helcoupling_" + str(hel_res),
                                                                                                               self.res_helicity_factors_dict[required_coupling])
                                        except :
                                                logging.error(("Resonance::set_HelAmp_and_couplings_BLS: the resonance helicity factor {} is required,"
                                                               + " please add it.").format(required_coupling))
                                                exit(1)
                                else :
		                        # coupling not needed: set it to 1.
                                        self.res_helicity_factors[hel_res] = Const(1.)
                else :
                        # loop over the M helicities
                        for hel_M in M_helicities :

                                self.res_helicity_factors[hel_M] = {}

                                # loop over the d1 helicities
                                for hel_d1 in d1_helicities :

                                        self.res_helicity_factors[hel_M][hel_d1] = {}

                                        # loop over the allowed resonance helicities
                                        for hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin) :
                                                
                                                required_coupling = str(hel_M) + ":" + str(hel_d1) + ":" + str(hel_res)
                                                
                                                if self.res_helicity_factors_dict != {} :
                                                        # I need to set the helicity coupling
                                                        try :
                                                                self.res_helicity_factors[hel_M][hel_d1][hel_res] = self.res_helicity_factors_dict[required_coupling]
                                                        except :
                                                                logging.error(("Resonance::set_HelAmp_and_couplings_BLS: the resonance helicity factors {} is required,"
                                                                               + "please add it.").format(required_coupling))
                                                                exit(1)
                                                else :
                                                        # coupling not needed: set it to 1.
                                                        self.res_helicity_factors[hel_M][hel_d1][hel_res] = Const(1.)

                return
        
        def set_BLS_M(self):
                
                #######
                # B_LS couplings of the M helicity amplitude
                #######

                # get all the possible B_LS couplings for the M decay
                S_values, L_values, self.BLS_M = get_all_possible_BLS(M.spin, self.spin, self.spectator.spin,
                                                                      parity_conserved = False)

                logging.debug("B_LS = {}".format(self.BLS_M))
                logging.debug("Resonance::set_BLS_M: S = {}".format(S_values))
                logging.debug("Resonance::set_BLS_M: L = {}".format(L_values))
                
                Lmin = min(L_values)

                allowedL = []
                
                # angular momentum barrier: restrict the less likely values of L to the
                # (L_min, ..., L_min + aboveLmin) values
                if self.aboveLmin_M >= 0:
                        allowedL = [L for L in L_values if L <= (Lmin + self.aboveLmin_M)]
                        
                logging.debug("Resonance:: set_BLS_M: L values of the M --> {} ## decay restricted to {}".format(self.name, allowedL))
                
                Smin = min(S_values)

                ##############

                # first, are we going to set all the helicity amplitudes?
                if self.fix_all_hel_amplitudes != None :
                        return
                
                # filter the allowed B_LS couplings
                self.BLS_M = Utils.filter_allowed_couplings(self.BLS_M, allowedL)
                
                # print some infos
                logging.debug("Resonance::set_BLS_M: allowedL = {}".format(allowedL))
                logging.info("Resonance::set_BLS_M: Lmin = {}, Smin = {}".format(Lmin, Smin))
                
                # set these damned M couplings
                self.BLS_M = self.set_BLS_couplings(self.BLS_M, self.BLS_couplings_dict_M, self.name + "__M")
                
                return
                
        def set_BLS_res(self):
                
                #######
                # B_LS couplings of the resonance helicity amplitude
                #######
                
                # get all the possible B_LS couplings for the resonance decay

                # L_values = orbital angular momenta between the resonance daughters
                S_values, L_values, self.BLS_res = get_all_possible_BLS(self.spin, self.a.spin, self.b.spin,
                                                                        parity_conserved = self.resdecay_parity_conservation,
                                                                        P_A = self.parity, P_B = self.a.parity, P_C = self.b.parity)

                # L_parent = angular orbital momentum between the resonance and the spectator particle
                self.L_parent_min = min(Utils.GetLvalues_res_spectator(self.spin, self.spectator.spin))
                
                Lmin = min(L_values)

                # L_res = angular orbital momentum between the resonance daughters
                self.L_res_min = Lmin

                logging.debug("Resonance::set_BLS_res: S = {}".format(S_values))
                logging.debug("Resonance::set_BLS_res: L = {}".format(L_values))

                allowedL = []
                
                # angular momentum barrier: restrict the less likely values of L to the
                # (L_min, ..., L_min + aboveLmin) values
                if self.aboveLmin_res >= 0:
                        allowedL = [L for L in L_values if L <= (Lmin + self.aboveLmin_res)]

                logging.debug("Resonance::set_BLS_res: L values of the {} decay restricted to {}".format(self.name, allowedL))

                Smin = min(S_values)

                #############

                # check: if I'm dealing with B resonances (for which the resonance helicity amplitudes
                # do not dependy by any helicities, since the B daughters are scalars)
                # and I'm setting by hand the resonance helicity amplitude
                # --> skip the settings of the BLS res couplings

                if (self.species == "B-type") and (self.fix_Bres_hel_amplitude != None) :
                        return
                
                #############

                # first, are we going to set all the helicity amplitudes?
                if self.fix_all_hel_amplitudes != None :
                        return
                
                # filter the allowed B_LS couplings
                self.BLS_res = Utils.filter_allowed_couplings(self.BLS_res, allowedL)

                # print some infos
                logging.debug("Resonance::set_BLS_res: allowedL = {}".format(allowedL))
                logging.info("Resonance::set_BLS_res: Lmin = {}, Smin = {}".format(Lmin, Smin))
                
                # set these damned M couplings
                self.BLS_res = self.set_BLS_couplings(self.BLS_res, self.BLS_couplings_dict_res, self.name + "__res")

                return

        # set the a single coupling
        def set_coupling(self, couplings_dict, coupling_name) :

                name_parameter_1 = ""
                name_parameter_2 = ""
                                
                if self.couplings_MagAndPhase :
                        name_parameter_1 = coupling_name + "_Mag"
                        name_parameter_2 = coupling_name + "_Phase"
                elif self.couplings_RealAndImag :
                        name_parameter_1 = coupling_name + "_Real"
                        name_parameter_2 = coupling_name + "_Imag"

                value_1 = -1
                value_2 = -1

                # some flags to say if the components are free fit parameters (= True)                                                                                               
                # or if are set equal to other parameters (= False)                                                                                                                  
                comp_1_ind = True
                comp_2_ind = True

                # search for the current coupling in the couplings of the decay, and set its components                                                                              
                
                try:
                        value_1 = couplings_dict[name_parameter_1]
                        value_2 = couplings_dict[name_parameter_2]
                        
                        # you might want to set some parameter (x) equal to another one (y):                                                                                         
                        # this is done if the value of x is a string,
                        # which is intended to be the name of y
                        if isinstance(value_1, basestring) :
                                value_1 = parse_operation_parameter(name_parameter_1, value_1) # couplings_dict[value_1]
                                comp_1_ind = False
                                
                        if isinstance(value_2, basestring) :
                                value_2 = parse_operation_parameter(name_parameter_2, value_2) # couplings_dict[value_2]
                                comp_2_ind = False
                                
                except :
                        logging.error(("Resonance::set_coupling: Error, the components of the {} coupling cannot be set."
                                       + " Check your config file or fit output").format(coupling_name))
                        logging.error("Those are the required components: {}, {}".format(name_parameter_1, name_parameter_2))
                        exit(1)
                        
                coupling = Coupling(name = coupling_name,
                                    comp_1 = value_1, comp_2 = value_2,
                                    couplings_MagAndPhase = self.couplings_MagAndPhase,
                                    couplings_RealAndImag = self.couplings_RealAndImag,
                                    comp_1_ind = comp_1_ind, comp_2_ind = comp_2_ind)
                
                # print out some infos about the BLS components                                                                                                                      
                if type(value_1) is tf.Tensor :
                        logging.info("Resonance::set_coupling: Set {}".format(name_parameter_1))
                if (type(value_1) is Optimisation.FitParameter) and comp_1_ind:
                        logging.info("Resonance::set_coupling: Setting {} as FREE fit parameter".format(name_parameter_1))
                elif (type(value_1) is Optimisation.FitParameter) and (comp_1_ind == False):
                        logging.info(("Resonance::set_coupling: Setting {} as fit parameter,"
                                      + " constrained to the value of {}").format(name_parameter_1, couplings_dict[name_parameter_1]))
                #else :
                #        logging.error("Resonance::set_coupling: Error: unrecognised type of the {} component.".format(name_parameter_1))
                #        exit(1)
                        
                if type(value_2) is tf.Tensor :
                        logging.info("Resonance::set_coupling: Set {}".format(name_parameter_2))
                elif (type(value_2) is Optimisation.FitParameter) and comp_2_ind :
                        logging.info("Resonance::set_coupling: Setting {} as FREE fit parameter".format(name_parameter_2))
		elif (type(value_2) is Optimisation.FitParameter) and (comp_2_ind == False):
                        logging.info(("Resonance::set_coupling: Setting {} as fit parameter,"
                                      + " constrained to the value of {}").format(name_parameter_2, couplings_dict[name_parameter_2]))
                #else :
                #        logging.error("Resonance::set_coupling: Error: unrecognised type of the {} component.".format(name_parameter_2))
                #        exit(1)

                return coupling
        
        # set these damned B_LS couplings
        def set_BLS_couplings(self, B_SL, couplings_dict, coupling_name):
	        
                # set these damned B_LS couplings, looping over the S, L and helicity combinations
                for S in B_SL.keys():
                        for L in B_SL[S].keys():
                                        
                                name_parameter = coupling_name + "_Bs" + str(S) + "l" + str(L)

                                # set the coupling
                                B_SL[S][L] = self.set_coupling(couplings_dict = couplings_dict,
                                                               coupling_name = name_parameter)
                                        
                # remove keys with empty values, just to be sure
                empty_keys = [k for k,v in B_SL.iteritems() if not v]

                for k in empty_keys:
                        logging.debug("Resonance::set_BLS_couplings: Found an empty key S = {}, removing it".format(k))
                        del B_SL[k]

                return B_SL
                
        # compute the complete matrix element for the M --> res spect, res --> a b decays,
        # for a given set of M and spectator helicities
        # here the spectator is d1
        def get_complete_matrix_element(self, coordinates, hel_M, hel_d1):
                
                M_decay = 0.
                
                # loop over the resonance helicities allowed by the hel_M, hel_d1 combo
                for hel_res in get_res_allowed_helicities(hel_M = hel_M, hel_d1 = hel_d1, res_spin = self.spin) :
                        
                        logging.debug("Resonance::get_complete_matrix_element: {}".format(self.name))
                        logging.debug("Resonance::get_complete_matrix_element: [hel_M = {}][hel_d1 = {}][hel_res = {}]".format(hel_M, hel_d1, hel_res))
                        
                        # get the M decay matrix
                        M_M = self.get_M_matrix_element(coordinates, hel_M = hel_M, hel_d1 = hel_d1, hel_res = hel_res)
                        
                        # get the res decay matrix
                        M_res = self.get_res_matrix_element(coordinates, hel_M = hel_M, hel_d1 = hel_d1, hel_res = hel_res)

                        # I need the resonance helicity factor only in the BLS representation
                        if self.use_BLS :
                                # get the resonance helicity factor
                                reshel_factor = CastComplex(self.res_helicity_factors[hel_res] if self.res_helicity_factors_reduced else self.res_helicity_factors[hel_M][hel_d1][hel_res])
                                
                                # sum the M and res matrix elements, multiplied by the resonance helicity factor
                                M_decay += reshel_factor * M_M * M_res
                        else :
                                M_decay += M_M * M_res
                                
                return M_decay

        def lineshape(self, msq, ma2 = None, mb2 = None):
                return CastComplex(Ones(msq))

        
##############################
#####  AResonance = d1 d2
##############################

# base class for the A -> d1 d2 resonance
class AResonance(Resonance):
        
        def initialise_resonance(self):
                
                # remember the convention:
                # M --> a b spectator
                
                self.a = d1  # to be fully consistent with the convention, this is assumed to have 1/2 spin
                self.b = d2  # to be fully consistent with the convention, this is assumed to have null spin
                self.spectator = d3

                # set the helicities of the initial and final-state particles
		self.set_helicities()

                # remember that in the current implementation we assume that:
                # spin(M) = 1/2, spin(d1) = 1/2, spin(d2) = 0, spin(d3) = 0
                # Check that everything is set as expected
                # You might want to remove this checks, in case you want to generalise the quantisation assumptions
                if ((max(self.M_helicities) != 1.) or (max(self.a_helicities) != 1.)
                    or (min(self.M_helicities) != -1.) or (min(self.a_helicities) != -1.)
                    or (self.b_helicities != [0.]) or (self.spectator_helicities != [0.])):
                        logging.error(("AResonance::initialise_resonance: Error: the spin assumptions are not respected!"
                                       + " M_helicities = {}, a_helicities = {}, b_helicities = {},"
                                       + " spectator_helicities = {}").format(self.M_helicities, self.a_helicities,
                                                                              self.b_helicities, self.spectator_helicities))
                        exit(1)
                
                self.species = "A-type"
                
                # set the couplings and the helicity amplitudes
                self.set_HelAmp_and_couplings()

        # build all the helicity amplitudes, with the generic representation of the couplings
        def set_genericHelAmp(self):

                ##########
                # build all the helicity amplitudes for the M --> res spectator
                # and res --> a b decays
                ##########
                
                self.genericHelAmp_M = {}
                self.genericHelAmp_res = {}

                ########
                # matrix elements of the M decay
                ########
                
                # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                for hel_res in get_all_possible_res_helicities(self.spin) :

                        # the mother particle M decay proceeds through weak decay,
                        # then for the M decay I don't have to consider parity conservation

                        # build the name of the helicity amplitude
                        helamp_name = self.name + "__M_hres" + str(hel_res)
                        
                        # set the coupling, which in the generic representation is the helicity amplitude
                        self.genericHelAmp_M[hel_res] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                          coupling_name = helamp_name)
                        
                ########
                # matrix elements of the resonance decay
                ########

                # now I might need to consider parity conservation,
                # for the res --> a b decay
                
                self.genericHelAmp_res[-1] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                               coupling_name = self.name + "__M_hd1-1")
                
                # now set the HelAmp with maximum hel_a (= +1) and check for parity conservation
                if self.resdecay_parity_conservation :
                        
                        # parity factor
                        parity_factor = self.parity * self.a.parity * self.b.parity * (-1)**(self.a.spin + self.b.spin - self.spin)
                        
                        self.genericHelAmp_res[+1] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                       coupling_name = self.name + "__M_hd11")
                        
                        logging.info(("AResonance::set_genericHelAmp: power of parity! Set genericHelAmp_res[hel_a = +1]"
                                      + " = genericHelAmp_res[hel_a = -1] / {}").format(hel_res))
                else :
                        # set the helicity amplitude in the 'standard' way
                        self.genericHelAmp_res[+1] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                       coupling_name = self.name + "__M_hd11")
                
                return

        # build all the helicity amplitudes, with the BLS representation of the couplings
        def set_BLSHelAmp(self):

                ##########
                # build all the helicity amplitudes for the M --> res spectator
                # and res --> a b decays
                ##########
                
                self.BLSHelAmp_M = {}
                self.BLSHelAmp_res = {}

                ########
                
                # if needed, set --> all <-- the helicity amplitudes!
                if self.fix_all_hel_amplitudes != None :
                        
                        # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                        for hel_res in get_all_possible_res_helicities(self.spin) :
                                
                                self.BLSHelAmp_M[hel_res] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)

                        # now fix the resonance amplitudes for both the Lc helicities
                        self.BLSHelAmp_res[-1] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)
                        self.BLSHelAmp_res[+1] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)

                        return
                
                ########
                # matrix elements of the M decay
                ########

                # here the spectator particle is a scalar (b):
                # the helicity amplitude only depends on the helicity of the resonance
                                        
                # check if I need to set the helicity amplitude by hand,
                # to fix the overall magnitude and phase conventions
                hel_res_tofix = None

                # for one specific helicity amplitude
		if self.fix_Lb_hel_amplitude != None :
                        
                        # get the resonance helicity that I have to fix
                        hel_res_tofix = int(self.fix_Lb_hel_amplitude[0])
                        
                # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                for hel_res in get_all_possible_res_helicities(self.spin) :
                        
                        # is this the helicity amplitude to fix?
                        if hel_res == hel_res_tofix :

                                self.BLSHelAmp_M[hel_res] = BLSHelicityAmplitude(fix_to_const = self.fix_Lb_hel_amplitude[1])

                                logging.info(("AResonance::set_BLSHelAmp: Fixed the Lb helicity amplitude"
                                              + " (hel_res = {}) = {}").format(hel_res, sess.run(self.fix_Lb_hel_amplitude[1])))
                                continue
                        
                        # the mother particle M decay proceeds through weak decay,
                        # then for the M decay I don't have to consider parity conservation
                        
                        # let's hardcode (why not...) the null values of J and helicity
                        self.BLSHelAmp_M[hel_res] = BLSHelicityAmplitude(J_mother = M.spin,
                                                                         J_1 = self.spin, hel_1 = hel_res,
                                                                         J_2 = 0, hel_2 = 0,
                                                                         B_SL = self.BLS_M)

                ########
                # matrix elements of the resonance decay
                ########
                
                # now I might need to consider parity conservation,
                # for the res --> a b decay
                
                # let's set the hel = -1 first, to eventually take care of parity conservation
                
                # let's hardcode (why not...) the null values of J and the helicity of c, which is a scalar
                self.BLSHelAmp_res[-1] = BLSHelicityAmplitude(J_mother = self.spin,
                                                              J_1 = 0, hel_1 = 0,
                                                              J_2 = self.a.spin, hel_2 = -1,
                                                              B_SL = self.BLS_res)
                
                # now set the HelAmp with maximum hel_a (= +1) and check for parity conservation
                if self.resdecay_parity_conservation :
                        
                        # parity factor
                        parity_factor = self.parity * self.a.parity * self.b.parity * (-1)**(self.a.spin + self.b.spin - self.spin)
                        
                        self.BLSHelAmp_res[+1] = BLSHelicityAmplitude(J_mother = self.spin,
                                                                      J_1 = 0, hel_1 = 0,
                                                                      J_2 = self.a.spin, hel_2 = -1,
                                                                      B_SL = {},
                                                                      fix_value = [self.BLSHelAmp_res[-1], parity_factor])
                        
                        logging.info(("AResonance::set_BLSHelAmp: power of parity! Set BLSHelAmp_res[hel_a = +1]"
                                      + " = BLSHelAmp_res[hel_a = -1] / {}").format(hel_res))
                else :
                        # set the helicity amplitude in the 'standard' way
                        
                        # let's hardcode (why not...) the null values of J and the helicity of c, which is a scalar
                        self.BLSHelAmp_res[+1] = BLSHelicityAmplitude(J_mother = self.spin,
                                                                      J_1 = 0, hel_1 = 0,    # J_c = 0, hel_c = 0
                                                                      J_2 = self.a.spin, hel_2 = +1,
                                                                      B_SL = self.BLS_res)
                                
                return
                                        
        # get the values of the amplitude variables
        def get_coordinates(self, phsp, datapoint):

                return phsp.ACoordinates(x = datapoint)
        
        # compute the matrix element of the M --> A d3 decay
        # for a given set of M and res helicities
        def get_M_matrix_element(self, coordinates, hel_M, hel_d1, hel_res):

                # check that the combination of hel_M, hel_d1 and hel_res makes sense
                if not (hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin)) :
                        return CastComplex(0.)
                
                # get the amplitude variables that I need
                theta_M_A = coordinates["theta_M_A"]

                M_M = CastComplex(0.)
                
                # remember that phid3min = 0. --> Polar(bla bla) = 1.

                # set the matrix element, depending on the representation of the couplings
                if self.use_BLS :
                        M_M = self.BLSHelAmp_M[hel_res].getvalue()
                else :
                        M_M = self.genericHelAmp_M[hel_res].value
                        
                M_M *= CastComplex(Kinematics.Wignerd(theta_M_A, M.spin, hel_M, hel_res))  # hel_d3 = 0
                
                logging.debug(("AResonance::get_M_matrix_element: BLSHelAmp_M[hel_res = {}]"
                               + " * d[spin_M = {}][hel_M = {}][hel_res = {}]").format(hel_res, M.spin, hel_M, hel_res))
                
                return M_M
        
        # compute the matrix element of the A --> d1 d2 decay
        # for a given set of resonance and d1 helicities
        def get_res_matrix_element(self, coordinates, hel_M, hel_d1, hel_res):

                # check that the combination of hel_M, hel_d1 and hel_res makes sense
                if not (hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin)) :
                        return CastComplex(0.)

                # get the amplitude variables that I need
                theta_A = coordinates["theta_A"]
                phi_d1_A = coordinates["phi_d1_A"]
                m2_d1d2 = coordinates["m2_d1d2"]

                M_A = CastComplex(0.)
                
                # set the matrix element, depending on the representation of the couplings
                if self.use_BLS	:
		        M_A = self.BLSHelAmp_res[hel_d1].getvalue()
		else :	
			M_A = self.genericHelAmp_res[hel_d1].value
                        
                M_A *= (CastComplex(Kinematics.Wignerd(theta_A, self.spin, hel_res, hel_d1)) *  # hel_d2 = 0
                        Interface.Polar(1.0, (hel_res * phi_d1_A)/ 2.) *  # divided by 2, because the helicity is doubled
                        self.lineshape(m2_d1d2, ma2 = self.ma2, mb2 = self.mb2)  # line shape factor of the A resonance
                )

                logging.debug(("AResonance::get_res_matrix_element: BLSHelAmp_res[hel_d1 = {}]"
                               + " * d[spin_res = {}][hel_res = {}][hel_d1 = {}]"
                               + " * Polar[hel_res = {}] ").format(hel_M, hel_d1, hel_res, self.spin, hel_res, hel_d1, hel_res))
                
                return M_A

# Breit Wigner version of the AResonance class
class AResonanceBreitWigner(AResonance):

        def set_res_typename(self):
                self.res_type = "AResonanceBreitWigner"
                
	def lineshape(self, msq, ma2 = None, mb2 = None):
		return Dynamics.BreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                     m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                     d_parent = self.d_parent, d_res = self.d_res,
                                                     L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)
        
# Sub-threshold Breit Wigner version of the AResonance class
class AResonanceSubThresholdBreitWigner(AResonance):

        def set_res_typename(self):
                self.res_type = "AResonanceSubThresholdBreitWigner"
                
        def lineshape(self, msq, ma2 = None, mb2 = None):
                return Dynamics.SubThresholdBreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                                 m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                                 d_parent = self.d_parent, d_res = self.d_res,
                                                                 L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

##############################
#####  BResonance = d2 d3
##############################

# base class for the B -> d2 d3 resonance
class BResonance(Resonance):
        
        def initialise_resonance(self):
                
                # remember the convention:
                # M --> a b spectator
	        self.a = d2  # to be fully consistent with the convention, this is assumed to have null spin
	        self.b = d3  # to be fully consistent with the convention, this is assumed to have null spin
	        self.spectator = d1

                # set the helicities of the initial and final-state particles
                self.set_helicities()
                
                # remember that in the current implementation we assume that:
	        # spin(M) = 1/2, spin(d1) = 1/2, spin(d2) = 0, spin(d3) = 0
	        # Check that everything is set as expected
	        # You might want to remove this checks, in case you want to generalise the quantisation assumptions
	        if ((max(self.M_helicities) != 1.) or (max(self.spectator_helicities) != 1.)
                    or (min(self.M_helicities) != -1.) or (min(self.spectator_helicities) != -1.)
                    or (self.a_helicities != [0.]) or (self.b_helicities != [0.])):
                        logging.error(("BResonance::initialise_resonance: Error: the spin assumptions are not respected!"
                                       + " M_helicities = {}, a_helicities = {}, b_helicities = {},"
                                       + " spectator_helicities = {}").format(self.M_helicities, self.a_helicities,
                                                                              self.b_helicities, self.spectator_helicities))
                        exit(1)
                        
                self.species = "B-type"
                
                # set the couplings and the helicity amplitudes
                self.set_HelAmp_and_couplings()

        # build all the helicity amplitudes, with the generic representation of the couplings
        def set_genericHelAmp(self):

                ##########
		# build all the helicity amplitudes for the M --> res spectator
                # and res --> a b decays
		##########
                
                self.genericHelAmp_M = {}
                self.genericHelAmp_res = {}

                # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                for hel_res in get_all_possible_res_helicities(self.spin) :
                        
                        ########
                        # matrix elements of the M decay
                        ########
                        
                        self.genericHelAmp_M[hel_res] = {}
                        
                        # loop over the d1 helicities, for the spin quantisation magic
                        for hel_d1_B in d1_helicities :
                                
                                # the mother particle M decay proceeds through weak decay,
                                # then I don't have to consider parity conservation

                                # build the name of the helicity amplitude
                                helamp_name = self.name + "__M_hd1B" + str(hel_d1_B)  + "_hres" + str(hel_res)
                                
                                self.genericHelAmp_M[hel_res][hel_d1_B] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                                            coupling_name = helamp_name)
                                
                        ########
                        # matrix elements of the resonance decay
                        ########
                        
                        # since res decays into two spinless particles,
                        # there is only one helicity amplitude to compute
                        # (i.e.: there is no dependence on the helicity of the daughter particles)
                        
                        self.genericHelAmp_res = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                   coupling_name = self.name + "__res")

                return

                
        # build all the helicity amplitudes, with the BLS representation of the couplings
        def set_BLSHelAmp(self):

                ##########
                # build all the helicity amplitudes for the M --> res spectator decay
                # and res --> a b decays
                ##########
                
                self.BLSHelAmp_M = {}
                self.BLSHelAmp_res = {}

                ##########

                # if needed, set --> all <-- the helicity amplitudes!
                if self.fix_all_hel_amplitudes != None :
                        
                        # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                        for hel_res in get_all_possible_res_helicities(self.spin) :

                                self.BLSHelAmp_M[hel_res] = {}
                                
                                # loop over the d1 helicities, for the spin quantisation magic
                                for hel_d1_B in d1_helicities :
				        
                                        self.BLSHelAmp_M[hel_res][hel_d1_B] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)

                                        logging.info("set_BLSHelAmp:: Fixed the amplitude BLSHelAmp_M[hel_res = {}][hel_d1 = {}] = {}".format(hel_res, hel_d1_B, sess.run(self.fix_all_hel_amplitudes)))
                                        
			# now fix the resonance amplitudes (no helicity dependence, since the resonance daughters are two scalars)
                        self.BLSHelAmp_res = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)

                        return
                
                ##########
                
                # here the Lb helicity amplitude depends on the resonance and d1 helicities
                
                # check if I need to set the Lb helicity amplitude by hand,
                # to fix the overall magnitude and phase conventions
                hel_res_tofix =	None
                hel_d1_B_tofix = None
                
                if self.fix_Lb_hel_amplitude != None :
                        
                        # get the resonance and d1_B helicities that I have to fix,
                        # splitting the string by the ":" delimiter
                        hel_res_tofix = int(self.fix_Lb_hel_amplitude[0].split(":")[0])
                        hel_d1_B_tofix = int(self.fix_Lb_hel_amplitude[0].split(":")[1])
                        
                # check if I have to set by hand the resonance helicity amplitude
                res_helicity_amplitude_tofix = None

                if self.fix_Bres_hel_amplitude != None:
                        # for the resonance helicity amplitudes of the B resonances
                        # there's no dependence by the helicities: it's "just" a complex number
                        res_helicity_amplitude_tofix = self.fix_Bres_hel_amplitude
                
                # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo                                                                                            
                for hel_res in get_all_possible_res_helicities(self.spin) :

                        ########
                        # matrix elements of the M decay
                        ########
                        
                        self.BLSHelAmp_M[hel_res] = {}
                        
                        # loop over the d1 helicities, for the spin quantisation magic
                        for hel_d1_B in d1_helicities :
                                        
                                # is this the helicity amplitude to fix?
                                if (hel_res == hel_res_tofix) and (hel_d1_B == hel_d1_B_tofix) :

                                        self.BLSHelAmp_M[hel_res][hel_d1_B] = BLSHelicityAmplitude(fix_to_const = self.fix_Lb_hel_amplitude[1])

                                        logging.info(("BResonance::set_BLSHelAmp: Fixed the Lb helicity amplitude"
                                                      + " (hel_res = {}, hel_d1_B = {}) = {}").format(hel_res, hel_d1_B, sess.run(self.fix_Lb_hel_amplitude[1])))
                                        continue

                                # the mother particle M decay proceeds through weak decay,
                                # then I don't have to consider parity conservation

                                self.BLSHelAmp_M[hel_res][hel_d1_B] = BLSHelicityAmplitude(J_mother = M.spin,
                                                                                           J_1 = self.spin, hel_1 = hel_res,
                                                                                           J_2 = d1.spin, hel_2 = hel_d1_B,
                                                                                           B_SL = self.BLS_M)

                ########
                # matrix elements of the resonance decay
                ########
                
                # since res decays into two spinless particles,
		# there is only one helicity amplitude to compute
		# (i.e.: there is no dependence on the helicity of the daughter particles)
                
                # do I have to set by hand the resonance helicity amplitude?
                if res_helicity_amplitude_tofix != None :
                        self.BLSHelAmp_res = BLSHelicityAmplitude(fix_to_const = res_helicity_amplitude_tofix)
                else :
                        # let's hardcode (why not...) the null values of J and the helicity of c, which is a scalar
                        self.BLSHelAmp_res = BLSHelicityAmplitude(J_mother = self.spin,
                                                                  J_1 = 0, hel_1 = 0,
                                                                  J_2 = 0, hel_2 = 0,
                                                                  B_SL = self.BLS_res)
                        
                return
                
        # get the values of the amplitude variables
        def get_coordinates(self, phsp, datapoint):
                
		return phsp.BCoordinates(x = datapoint)

        
        # compute the matrix element of the M --> B d1 decay
        # for a given set of M, B and d1 helicities
        def get_M_matrix_element(self, coordinates, hel_M, hel_d1, hel_res):

                # check that the combination of hel_M, hel_d1 and hel_res makes sense
                if not (hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin)) :
                        return CastComplex(0.)

                # get the amplitude variables that I need
	        theta_M_B = coordinates["theta_M_B"]
                phi_d1_B = coordinates["phi_d1_B"]
                theta_d1_AB = coordinates["theta_d1_AB"]
                
                M_M = CastComplex(0.)

                logging.debug(("BResonance::get_M_matrix_element: spin quantisation magic happening!"
                               + "Sum over hel_d1_B = {}").format(d1_helicities))
                
                # I have to perform this sum because the d1 quantisation axes are different
                # for the A and B decay chains
                for hel_d1_B in d1_helicities :

                        M_M_temp = CastComplex(0.)
                        
                        # set the matrix element, depending on the representation of the couplings
	                if self.use_BLS :
                                M_M_temp = self.BLSHelAmp_M[hel_res][hel_d1_B].getvalue()
                        else :
                                M_M_temp = self.genericHelAmp_M[hel_res][hel_d1_B].value

                        M_M_temp *= (
                                # factor to align the d1 quantisation axes
                                CastComplex(Kinematics.Wignerd(theta_d1_AB, d1.spin, hel_d1_B, hel_d1)) *
                                
                                CastComplex(Kinematics.Wignerd(theta_M_B, M.spin, hel_M, hel_res - hel_d1_B)) *
                                Interface.Polar(1.0, (hel_M * phi_d1_B)/2.)  # divided by 2, because the helicity is doubled
                        )

                        # update the global matrix element
                        M_M += M_M_temp
                        
                        logging.debug(("BResonance::get_M_matrix_element: d[spin_d1 = {}][hel_d1_B = {}][hel_d1 = {}]"
                                       + " * BLSHelAmp_M[hel_res = {}][hel_d1_B = {}]"
                                       + " * d[spin_M = {}][hel_M = {}][hel_res - hel_d1_B = {}]"
                                       + " * Polar[hel_M = {}]" ).format(d1.spin, hel_d1_B, hel_d1, hel_res, hel_d1_B,
                                                                         M.spin, hel_M, hel_res - hel_d1_B, hel_M))
                        
                return M_M

        
        # compute the matrix element of the B --> d2 d3 decay
        # for a given B helicity value
        def get_res_matrix_element(self, coordinates, hel_M, hel_d1, hel_res):

                # check that the combination of hel_M, hel_d1 and hel_res makes sense
                if not (hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin)) :
                        return CastComplex(0.)

                logging.debug("hel_M = {}, hel_d1 = {}, hel_res = {}".format(hel_M, hel_d1, hel_res))
                
                # get the amplitude variables that I need
                theta_B = coordinates["theta_B"]
                phi_d2_B = coordinates["phi_d2_B"]
                m2_d2d3 = coordinates["m2_d2d3"]

                M_B = CastComplex(0.)
                
                # set the matrix element, depending on the representation of the couplings
                if self.use_BLS :
                        M_B = self.BLSHelAmp_res.getvalue()
                else :
                        M_B = self.genericHelAmp_res.value
                                
                M_B *= (CastComplex(Kinematics.Wignerd(theta_B, self.spin, hel_res, 0)) *   # hel_d2 = 0, hel_d3 = 0
                       Interface.Polar(1.0, (hel_res * phi_d2_B)/2.) *  # divided by 2, because the helicity is doubled
                       self.lineshape(m2_d2d3, ma2 = self.ma2, mb2 = self.mb2) # line shape factor of the B resonance
                )

                logging.debug(("BResonance::get_res_matrix_element: BLSHelAmp_res[hel_M = {}][hel_d1 = {}][hel_res = {}]"
                               + " * d[res_spin = {}][hel_res = {}][0]"
                               + " * Polar[hel_res = {}]" ).format(hel_M, hel_d1, hel_res, self.spin, hel_res, hel_res))
                
                return M_B
                        
# Breit Wigner version of the BResonance class
class BResonanceBreitWigner(BResonance):

        def set_res_typename(self):
                self.res_type = "BResonanceBreitWigner"
                
        def lineshape(self, msq, ma2 = None, mb2 = None):
                return Dynamics.BreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                     m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                     d_parent = self.d_parent, d_res = self.d_res,
                                                     L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Sub-threshold Breit Wigner version of the BResonance class
class BResonanceSubThresholdBreitWigner(BResonance):

        def set_res_typename(self):
                self.res_type = "BResonanceSubThresholdBreitWigner"
                
        def lineshape(self, msq, ma2 = None, mb2 = None):
                return Dynamics.SubThresholdBreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                                 m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                                 d_parent = self.d_parent, d_res = self.d_res,
                                                                 L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Flatte version of the BResonance class
# Assume the dominant channel is Ds pi0, i.e. this is the Ds0(2317)
# Construct just like BResonanceBreitWigner, but give width as a two-element list or tuple of tf tensors
class BResonanceFlatte(BResonance):

        def set_res_typename(self):
                self.res_type = "BResonanceFlatte"
        
        def lineshape(self, msq, ma2 = Ds.mass, mb2 = pi0.mass):

                # go to hell, Tensorflow
                ma2 = Const(1.968)
                mb2 = Const(0.145)
                
                return Dynamics.FlatteLineShape(m2 = msq, m0 = self.mass, g1 = self.width[0], g2 = self.width[1]*self.width[0],
                                                m_parent = self.m_ancestor,
                                                ma1 = self.a.mass, mb1 = self.b.mass, ma2 = ma2, mb2 = mb2,
                                                mc = self.spectator.mass,
                                                d_parent = self.d_parent, d_res = self.d_res,
                                                L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Polynomial version of the BResonance class
class BResonancePolynomial(BResonance):

        def set_res_typename(self):
                self.res_type = "BResonancePolynomial"

        def lineshape(self, msq, ma2 = None, mb2 = None):
                return Dynamics.PolynomialLineShape(m2 = msq, coefficients = self.pol_coefficients)

# KMatrix (!!)
class BResonanceKMatrix(BResonance):
        
        def set_res_typename(self):
                self.res_type = "BResonanceKMatrix"

        def lineshape(self, msq, ma2 = None, mb2 = None):
                
                # initialise the KMatrix
                self.Kmat = KMatrix.KMatrix(s = msq,
                                            num_channels = self.Kmat__num_channels,
                                            channel = self.Kmat__channel,
                                            resonance_masses = self.Kmat__resonance_masses,
                                            num_res = self.Kmat__num_res,
                                            alpha_couplings = self.Kmat__alpha_couplings,
                                            g_couplings = self.Kmat__g_couplings,
                                            width_factors = self.Kmat__width_factors,
                                            daughters_masses = self.Kmat__daughters_masses,
                                            L_channels = self.Kmat__L_channels,
                                            background_terms = self.Kmat__background_terms)

                # get the amplitude for the requested channel,
                # and cast it to complex
                amplitude = CastComplex(self.Kmat.get_amplitude(s = msq, channel = self.Kmat__channel))

                # now it's time to get the Blatt-Weisskop factors for the line shape
                m = Sqrt(msq)

                # the Blatt-Weisskopf factors depend on the mass of the resonance, to set an intrinsic scale q0, p0
                # here there are multiple resonances described, soooo... use their average mass, and who cares
                resonance_mass = 0.

                for mass in self.Kmat__resonance_masses :
                        resonance_mass += mass

                resonance_mass /= len(self.Kmat__resonance_masses)
                
                # and also take its absolute value,
                # since the resonance masses here where complex quantities
                resonance_mass = Abs(resonance_mass)
                
                # momentum of the resonance, decaying to the m1 and m2
                q   = Kinematics.TwoBodyMomentum(m, self.a.mass, self.b.mass)
                q_0 = Kinematics.TwoBodyMomentum(resonance_mass, self.a.mass, self.b.mass)

                # Blatt-Weisskopf factor for the resonance decay
                BWfactor_resonance = Dynamics.BlattWeisskopfFormFactor(q, q_0, self.d_res, self.L_res_min/2)

                # momentum of the parent, decaying to the resonance and the spectator
                p   = Kinematics.TwoBodyMomentum(self.m_ancestor, m, self.spectator.mass)
                p_0 = Kinematics.TwoBodyMomentum(self.m_ancestor, resonance_mass, self.spectator.mass)

                # Blatt-Weisskopf factor for the parent decay
                BWfactor_parent = Dynamics.BlattWeisskopfFormFactor(p, p_0, self.d_parent, self.L_parent_min/2)
                
                # return the amplitude, multiplied by the Blatt-Weisskopf factors
                return amplitude * CastComplex(BWfactor_resonance) * CastComplex(BWfactor_parent)
                
##############################
#####  CResonance = d1 d3
##############################

# base class for the B -> d2 d3 resonance
class CResonance(Resonance):
        
        def initialise_resonance(self):
                
                # remember the convention:
                # M --> a b spectator
                self.a = d1  # to be fully consistent with the convention, this is assumed to have 1/2 spin
                self.b = d3  # to be fully consistent with the convention, this is assumed to have null spin
                self.spectator = d2

                # set the helicities of the initial and final-state particles
		self.set_helicities()

                # remember that in the current implementation we assume that:
                # spin(M) = 1/2, spin(d1) = 1/2, spin(d2) = 0, spin(d3) = 0
		# Check that everything is set as expected
                # You might want to remove this checks, in case you want to generalise the quantisation assumptions
                if ((max(self.M_helicities) != 1.) or (max(self.a_helicities) != 1.)
                    or (min(self.M_helicities) != -1.) or (min(self.a_helicities) != -1.)
		    or (self.b_helicities != [0.]) or (self.spectator_helicities != [0.])):
                	logging.error(("CResonance::initialise_resonance: Error: the spin assumptions are not respected!"
                                       + " M_helicities = {}, a_helicities = {}, b_helicities = {},"
                                       + " spectator_helicities = {}").format(self.M_helicities, self.a_helicities,
                                                                              self.b_helicities, self.spectator_helicities))
                        exit(1)
                        
                self.species = "C-type"
                
                # set the couplings and the helicity amplitudes
                self.set_HelAmp_and_couplings()

        # build all the helicity amplitudes, with the generic representation of the couplings
        def set_genericHelAmp(self):

                ##########
                # build all the helicity amplitudes for the M --> res spectator
                # and res --> a b decays
                ##########
                
                self.genericHelAmp_M = {}
                self.genericHelAmp_res = {}

                ########
                # matrix elements of the M decay
                ########

                # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                for hel_res in get_all_possible_res_helicities(self.spin) :
                        
                        # the mother particle M decay proceeds through weak decay,
                        # then for the M decay I don't have to consider parity conservation

                        # build the name of the helicity amplitude
                        helamp_name = self.name + "__M_hres" + str(hel_res)
                        
                        # set the coupling, which in the generic representation is the helicity amplitude
                        self.genericHelAmp_M[hel_res] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                          coupling_name = helamp_name)

                ########
                # matrix elements of the resonance decay
                ########
                
                # now I might need to consider parity conservation,
                # for the res --> a b decay
                
                self.genericHelAmp_res[-1] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                               coupling_name = self.name + "__M_hd1C-1")

                # now set the HelAmp with maximum hel_a (= +1) and check for parity conservation
                if self.resdecay_parity_conservation :
                        
                        # parity factor
                        parity_factor = self.parity * self.a.parity * self.b.parity * (-1)**(self.a.spin + self.b.spin - self.spin)
                        
                        self.genericHelAmp_res[+1] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                       coupling_name = self.name + "__M_hd1C1")
                        
                        logging.info(("CResonance::set_genericHelAmp: power of parity! Set genericHelAmp_res[hel_a = +1]"
                                      + " = genericHelAmp_res[hel_a = -1] / {}").format(hel_res))
                else :
                        # set the helicity amplitude in the 'standard' way
                        self.genericHelAmp_res[+1] = self.set_coupling(couplings_dict = self.generic_couplings_dict,
                                                                       coupling_name = self.name + "__M_hd1C1")

                return
                        
        # build all the helicity amplitudes, with the BLS representation of the couplings
        def set_BLSHelAmp(self):
                
                ##########
                # build all the helicity amplitudes for the M --> res spectator
                # and res --> a b decays
                ##########
                
                self.BLSHelAmp_M = {}
                self.BLSHelAmp_res = {}

                ##########

                # if needed, set --> all <-- the helicity amplitudes!
                if self.fix_all_hel_amplitudes != None :
                        
                        # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                        for hel_res in get_all_possible_res_helicities(self.spin) :
                                
		                self.BLSHelAmp_M[hel_res] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)
                                
                        # now fix the resonance amplitudes for both the Lc helicities
			self.BLSHelAmp_res[-1] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)
                        self.BLSHelAmp_res[+1] = BLSHelicityAmplitude(fix_to_const = self.fix_all_hel_amplitudes)

                        return
                
                ########
                # matrix elements of the M decay
                ########

                # here the spectator particle is a scalar (b):
		# the helicity amplitude only depends on the helicity of the resonance
                
                # check if I need to set the helicity amplitude by hand,
                # to fix the overall magnitude and phase conventions
                hel_res_tofix = None

                if self.fix_Lb_hel_amplitude != None :

                        # get the resonance helicity that I have to fix
                        hel_res_tofix = int(self.fix_Lb_hel_amplitude[0])
                        
                # loop over the resonance helicities which are allowed by the hel_M, hel_d1 combo
                for hel_res in get_all_possible_res_helicities(self.spin) :
                        
                        # is this the helicity amplitude to fix?
			if hel_res == hel_res_tofix :

                                self.BLSHelAmp_M[hel_res] = BLSHelicityAmplitude(fix_to_const = self.fix_Lb_hel_amplitude[1])

                                logging.info(("CResonance::set_BLSHelAmp: Fixed the Lb helicity amplitude"
                                              + " (hel_res = {}) = {}").format(hel_res, sess.run(self.fix_Lb_hel_amplitude[1])))
                                continue

                        # the mother particle M decay proceeds through weak decay,
                        # then for the M decay I don't have to consider parity conservation
                        
                        # let's hardcode (why not...) the the null values of J and helicity
                        self.BLSHelAmp_M[hel_res] = BLSHelicityAmplitude(J_mother = M.spin,
                                                                         J_1 = self.spin, hel_1 = hel_res,
                                                                         J_2 = 0, hel_2 = 0,
                                                                         B_SL = self.BLS_M)

                ########
                # matrix elements of the resonance decay
                ########
                
                # now I might need to consider parity conservation,
                # for the res --> a b decay
                
                # let's set the hel_d1 = -1 first, to eventually take care of parity conservation    
                # remember that here there's hel_d1 dependence only because of quantisation magic

                # let's hardcode (why not...) the null values of J and the helicity of c, which is a scalar
                self.BLSHelAmp_res[-1] = BLSHelicityAmplitude(J_mother = self.spin,
                                                              J_1 = d1.spin, hel_1 = -1,
                                                              J_2 = 0, hel_2 = 0,
                                                              B_SL = self.BLS_res)
                        
                # now set the HelAmp with maximum hel_d1 (= +1) and check for parity conservation
                if self.resdecay_parity_conservation :
                        
                        # parity factor
                        parity_factor = self.parity * self.a.parity * self.b.parity * (-1)**(self.a.spin + self.b.spin - self.spin)
                        
                        self.BLSHelAmp_res[+1] = BLSHelicityAmplitude(J_mother = self.spin,
                                                                      J_1 = d1.spin, hel_1 = +1,
                                                                      J_2 = 0, hel_2 = 0,
                                                                      B_SL = {},
                                                                      fix_value = [self.BLSHelAmp_res[-1], parity_factor])
                        
                        logging.info(("CResonance::set_BLSHelAmp: power of parity! Set BLSHelAmp_res[hel_a = +1]"
                                      + " = BLSHelAmp_res[hel_a = -1] / {}").format(parity_factor))
                else :
                        # set the helicity amplitude in the 'standard' way

                        # let's hardcode (why not...) the null values of J and the helicity of c, which is a scalar
                        self.BLSHelAmp_res[+1] = BLSHelicityAmplitude(J_mother = self.spin,
                                                                      J_1 = d1.spin, hel_1 = +1,
                                                                      J_2 = 0, hel_2 = 0,
                                                                      B_SL = self.BLS_res)
                
                return                                
        
        # get the values of the amplitude variables
        def get_coordinates(self, phsp, datapoint):

                return phsp.CCoordinates(x = datapoint)

        # compute the matrix element of the M --> C d2 decay
        # for a given set of M and C helicities
        def get_M_matrix_element(self, coordinates, hel_M, hel_res, hel_d1):

                # check that the combination of hel_M, hel_d1 and hel_res makes sense
                if not (hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin)) :
                        return CastComplex(0.)

                # get the amplitude variables that I need
                theta_M_C = coordinates["theta_M_C"]
                phi_d2_C = coordinates["phi_d2_C"]

                M_M = CastComplex(0.)

                # set the matrix element, depending on the representation of the couplings                                                                                                   
                if self.use_BLS :
                        M_M = self.BLSHelAmp_M[hel_res].getvalue()
                else :
                        M_M = self.genericHelAmp_M[hel_res].value

                M_M *= (CastComplex(Kinematics.Wignerd(theta_M_C, M.spin, hel_M, hel_res)) *  # hel_d2 = 0
                       Interface.Polar(1.0, (hel_M * phi_d2_C)/2.)  # divided by 2, because the helicity is doubled
                )

                logging.debug(("CResonance::get_M_matrix_element: BLSHelAmp_M[hel_res = {}]"
                               + " * d[spin_M = {}][hel_M = {}][hel_res = {}]"
                               + " * Polar[hel_M = {}]").format(hel_res, M.spin, hel_M, hel_res, hel_M))
                
                return M_M

        # compute the matrix element of the C --> d1 d3 decay
        # for a given set of C and d1 helicities
        def get_res_matrix_element(self, coordinates, hel_M, hel_d1, hel_res):

                # check that the combination of hel_M, hel_d1 and hel_res makes sense
                if not (hel_res in get_res_allowed_helicities(hel_M, hel_d1, self.spin)) :
                        return CastComplex(0.)

                # get the amplitude variables that I need
                theta_C = coordinates["theta_C"]
                phi_d1_C = coordinates["phi_d1_C"]
                theta_d1_AC = coordinates["theta_d1_AC"]
                m2_d1d3 = coordinates["m2_d1d3"]
                
                M_C = CastComplex(0.)

                logging.debug(("CResonance::get_res_matrix_element: spin quantisation magic happening!"
                               + "Sum over hel_d1_C = {}").format(d1_helicities))
                                
                # I have to perform this sum because the d1 quantisation axes are different
                # for the A and C decay chains
                for hel_d1_C in d1_helicities :

                        M_C_temp = CastComplex(0.)
                        
                        # set the matrix element, depending on the representation of the couplings
                        if self.use_BLS :
                                M_C_temp = self.BLSHelAmp_res[hel_d1_C].getvalue()
                        else :
                                M_C_temp = self.genericHelAmp_res[hel_d1_C].value
                        
                        M_C_temp *= (
                                # factor to align the d1 quantisation axes
                                CastComplex(Kinematics.Wignerd(theta_d1_AC, d1.spin, hel_d1_C, hel_d1)) *
                                
                                CastComplex(Kinematics.Wignerd(theta_C, self.spin, hel_res, hel_d1_C)) *
                                
                                Interface.Polar(1.0, (hel_res * phi_d1_C)/2.) *  # divided by 2, because the helicity is doubled
                                self.lineshape(m2_d1d3, ma2 = self.ma2, mb2 = self.mb2) # line shape factor of the C resonance
                        )
                        
                        # update the global matrix element
                        M_C += M_C_temp
                        
                        logging.debug(("CResonance::get_res_matrix_element: d[spin_d1 = {}][hel_d1_C = {}][hel_d1 = {}]"
                                       + " * BLSHelAmp_res[hel_d1_C = {}]"
                                       + " * d[res_spin = {}][hel_res = {}][hel_d1_C = {}]"
                                       + " * Polar[hel_res = {}]").format(d1.spin, hel_d1_C, hel_d1, hel_d1_C,
                                                                          self.spin, hel_res, hel_d1_C, hel_res))
                        
                return M_C
        

# Breit Wigner version of the CResonance class
class CResonanceBreitWigner(CResonance):

        def set_res_typename(self):
                self.res_type = "CResonanceBreitWigner"

        def lineshape(self, msq, ma2 = None, mb2 = None):
                return Dynamics.BreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                     m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                     d_parent = self.d_parent, d_res = self.d_res,
                                                     L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Sub-threshold Breit Wigner version of the CResonance class
class CResonanceSubThresholdBreitWigner(CResonance):

        def set_res_typename(self):
                self.res_type = "CResonanceSubThresholdBreitWigner"

        def lineshape(self, msq, ma2 = None, mb2 = None):
                return Dynamics.SubThresholdBreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                                 m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                                 d_parent = self.d_parent, d_res = self.d_res,
                                                                 L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Flatte version of the CResonance class
# Assume the dominant channel is Xic pi, i.e. this is the Xic(2790)
# Construct just like CResonanceBreitWigner, but give width as a two-element list or tuple of tf tensors
class CResonanceFlatte(CResonance):

        def set_res_typename(self):
                self.res_type = "CResonanceFlatte"
                
        def lineshape(self, msq, ma2 = Xic_2790.mass, mb2 = K.mass):
                return Dynamics.FlatteLineShape(m2 = msq, m0 = self.mass, g1 = self.width[0], g2 = self.width[1]*self.width[0],
                                                m_parent = self.m_ancestor,
                                                ma1 = self.a.mass, mb1 = self.b.mass, ma2 = ma2, mb2 = mb2,
                                                mc = self.spectator.mass,
                                                d_parent = self.d_parent, d_res = self.d_res,
                                                L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

##############################
#####  Non-resonant contribution
##############################

class NonResonant():

        def __init__(self,
                     nonres_size = Const(0.),
                     colour = 1, linewidth = 2, linestyle = 1, fillstyle = 0):

                logging.info("")
                logging.info("NonResonant:: Creating a non resonant contribution")
                
                # some style
                self.colour = colour
                self.linewidth = linewidth
                self.linestyle = linestyle
                self.fillstyle = fillstyle

                self.nonres_size = nonres_size
                
                name_parameter = "nonres_size"

                # I can set the non-resonant size from the (non)-resonance properties,
                # or from a set of constant values (i.e., the set of fitted values provided externally)
                # The latter is mainly for plotting
                
                # So, check if I have to override the property with the const value
                if fitted_values != [] :
                        try :
                                self.nonres_size = Const(fitted_values[name_parameter])
                        except :
                                logging.debug("all fine")
                
                # printout some infos
                if type(self.nonres_size) is tf.Tensor :
                        logging.info("NonResonant:: Set the non resonant size = {}".format(sess.run(self.nonres_size)))
                elif type(self.nonres_size) is Optimisation.FitParameter :
                        logging.info("NonResonant:: Setting the non resonant size as fit parameter")

        # save the resonant size to an output file
        def SaveNonResSize(self, outFile) :
                
                outFile.write("nonres_size\t" + str(sess.run(self.nonres_size)) + "\t-999.\n")
                
                return
        
        def density(self):
                return Interface.Density(CastComplex(self.nonres_size))
        
##############################
#####  Matrix element
##############################

# extract the masses and angles from the datapoint
# and do the incoherent sum over initial/final state particle helicities and the coherent sum over resonances
def MatrixElement(phsp, datapoint, switches,
                  component_index = -1, resonances = [],
                  non_res = None,
                  M_d1_hel_ = None,
                  eff_shape = None, eff_shape_2 = None,
                  eff_var_1 = None, eff_var_2 = None):

        global M_d1_hel

        # (eventually) parse the M and d1 helicity factors
        if M_d1_hel_ is not None :
                M_d1_hel = Parse_M_d1_helicities(M_d1_hel_)

                # print the helicity factors of M and d1
                logging.info("MatrixElement:: M_d1_hel = {}".format(sess.run(M_d1_hel)))
        
        # total density
        density = Const(0.)
        
        # add the density of a non-resonant contribution
        # its switch is the last element of the switches list: this is evil
        if non_res != None :
                if component_index < 0 or component_index is (len(resonances)) :
                        density += switches[-1] * non_res.density()
        
	# incoherent sum over the initial and final state particle helicities
        
        # sum over the M helicities (+- 1)
	for hel_M in M_helicities:
                
                # sum over the d1 helicities (+- 1)
		for hel_d1 in d1_helicities:
			
			amplitude = CastComplex(Const(0.))
                        
                        # add all the amplitudes
                        if component_index < 0:

                                logging.debug("")
                                logging.debug("hel_M = {}, hel_d1 = {}".format(hel_M, hel_d1))

                                # loop over the resonances
                                for res, whatever in zip(resonances, switches) :
                                        
                                        # get the values of the amplitude variables
                                        coordinates = res.get_coordinates(phsp, datapoint)
                                        
                                        # add the amplitude, already summed over the resonance helicities
                                        amplitude += CastComplex(whatever) * res.get_complete_matrix_element(coordinates, hel_M = hel_M, hel_d1 = hel_d1)
                                        
                        # speed up the graph building if you have only one component
                        elif component_index < len(resonances) :
                                # get the values of the amplitude variables
                                coordinates = resonances[component_index].get_coordinates(phsp, datapoint)
                                
                                # get the amplitude for a single resonance, already summed over the resonance helicities
                                amplitude = CastComplex(switches[component_index]) * \
                                            resonances[component_index].get_complete_matrix_element(coordinates, hel_M = hel_M, hel_d1 = hel_d1)

                        # Interface.Density = tf.abs(ampl)**2
                        density_temp = Interface.Density(amplitude)

                        # do I have to take into account the M polarisation?
                        if (M_d1_hel != None) and resonances[0].use_BLS :
                                density_temp *= M_d1_hel[hel_M][hel_d1]
                                
                        density += density_temp
        
        # add the efficiency contributions to the PDF
        if eff_shape != None :
                # 2D efficiency look-up table

                # hopefully "eval" works correctly...
                density *= eff_shape.shape(tf.stack([eval("phsp." + eff_var_1 + "(datapoint)"),
                                                     eval("phsp." + eff_var_2 + "(datapoint)")],
                                                    axis = 1))
                    
                logging.info("MatrixElement:: correcting by the first efficiency")
                
                # 3D efficiency look-up table: ROOT has not Smooth method for TH3D...
                #density *= eff_shape.shape(tf.stack([phase_space.cosThetaM(datapoint),
                #                                     phase_space.phid1(datapoint),
                #                                    (phase_space.Md1d2(datapoint))**2],
                #                                    axis = 1))
                
        if eff_shape_2 != None :
		# 2D efficiency look-up table
                
                # hopefully "eval" works correctly...
                density *= eff_shape_2.shape(tf.stack([eval("phsp." + eff_var_1 + "(datapoint)"),
                                                       eval("phsp." + eff_var_2 + "(datapoint)")],
                                                      axis = 1))

                logging.info("MatrixElement:: correcting by the second efficiency")
        
	return density

#########

# compute the interference amplitude between two states
def MatrixElement_interference(phsp, datapoint,
                               component_indeces = [], resonances = [],
                               M_d1_hel_ = None):

        global M_d1_hel
        
        # (eventually) parse the M and d1 helicity factors
        if M_d1_hel_ is not None :
                M_d1_hel = Parse_M_d1_helicities(M_d1_hel_)
        
                # print the helicity factors of M and d1
                logging.debug("MatrixElement_interference:: M_d1_hel = {}".format(sess.run(M_d1_hel)))
        
        # total density
        density = CastComplex(0.)
        
        # incoherent sum over the initial and final state particle helicities

        # sum over the M helicities
        for hel_M in M_helicities:

                # sum over the d1 helicities
                for hel_d1 in d1_helicities:

                        # list of all interfering amplitudes
                        amplitudes = []
                        
                        # amplitude of a single resonance
                        single_amplitude = CastComplex(Const(0.))

                        # loop over the indeces of the interfering indeces
                        for index in component_indeces :

                                # get the values of the amplitude variables
                                coordinates = resonances[index].get_coordinates(phsp, datapoint)
                                
                                # get the amplitude of a single resonance, already summed over the resonance helicities
                                single_amplitude = resonances[index].get_complete_matrix_element(coordinates, hel_M = hel_M, hel_d1 = hel_d1)
                                                                        
                                amplitudes.append(single_amplitude)

                        # ok, now is getting fishy:
                        # I assume we only have interference between two states
                        # just to be sure, check it
                        if len(amplitudes) != 2:
                                logging.error(("MatrixElement_interference:: Error, the interference amplitudes are not 2."
                                              + " len(amplitudes) = {}.").format(len(amplitudes)))
                                exit(1)
                                
                        density_temp = tf.multiply(amplitudes[0], Interface.Conjugate(amplitudes[1]))

                        # do I have to take into account the M polarisation?
                        if (M_d1_hel != None) and resonances[0].use_BLS :
                                density_temp *= CastComplex(M_d1_hel[hel_M][hel_d1])
                                
                        # multiply the first amplitude by the complex conjugate of the second one
                        density += density_temp
                        
        return density
