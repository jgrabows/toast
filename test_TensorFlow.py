
# test that all the libraries that we need are installed
import math
import sys
import timeit

print "\n--> The generic Python libaries that we need are installed.\n"

# test the TensorFlow installation
import tensorflow as tf

message = tf.constant("\n--> TensorFlow works!\n")
sess = tf.Session()

print sess.run(message)

# test the Root installation
from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT

message = tf.constant("\n--> The ROOT libraries are imported.\n--> Everything is correctly set, that's amazing!\n")
sess = tf.Session()

print sess.run(message)
