
# TOAST

A new fitting framework for amplitude analysis. Yes, one more. But this is based on Tensorflow!

Before starting to use and/or modify this framework,
please be aware that this is a open source software released under LGPL v3 license.
By using this software, you agree to undergo this license.
For more information please refer to the license section available in this repository. 

# Main features

The TOAST fitting framework makes use of the isobar model for describing 3-body decays, implemented using the helicity formalism.
A full description of the decays decompositions and of the amplitude model is reported in [this extract](amplitude_model.pdf)
of CERN-THESIS-.

The framework is able to handle decays of particles with spin in two scalars + third particle with spin, as example Lb --> Lc D0 K.
Development is ongoing for generalising it.

# Installation

Instructions how to install the required software environment are provided [here](INSTALLATION.md).

# Usage

Some general instructions will arrive soon, but you can already enjoy [this section](FITTING.md).