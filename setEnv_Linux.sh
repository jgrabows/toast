
# Example script to configure the environment on a (local) Linux installation
# Alessio Piucci

# activate snake
source activate snake

# set the work directory, only if needed
if [ -z "$CI_WORKDIR_AMPL" ]
then
    export CI_WORKDIR_AMPL=/tmp/Lb2LcD0Kampl_work
fi

# for TensorFlow: add the TH2A library path to the folders searched by ROOT
export LIBPATH=$LIBPATH:`pwd`/build/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/build/lib/

# add some folders where the required Python modules are placed
export PYTHONPATH=$PYTHONPATH:`pwd`/externals/TensorFlowAnalysis/TensorFlowAnalysis/:`pwd`/config/:`pwd`/config/amplitude_fits/:`pwd`/include/:`pwd`/config/amplitude_fits/states

# test the TensorFlow installation
python2.7 test_TensorFlow.py
